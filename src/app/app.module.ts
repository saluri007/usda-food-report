import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';
import { FavPageModule } from '../pages/fav/fav.module';
 
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { DetailsViewPage } from '../pages/details-view/details-view';
import { RestApiProvider } from '../providers/rest-api/rest-api';



@NgModule({
  declarations: [
    MyApp,
    HomePage,
    DetailsViewPage,
    
  
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FavPageModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    DetailsViewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    RestApiProvider
  ]
})
export class AppModule {}
