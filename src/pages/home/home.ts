import { Component } from '@angular/core';
import { NavController, LoadingController, MenuController } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api/rest-api';
import { DetailsViewPage } from '../details-view/details-view';
import { FavPage} from '../fav/fav';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  foods: string[];
  errorMessage: string;
  descending: boolean = false;
  order: number;
  terms: string;
  column: string = 'name';
  favFoods : any[] = [];
  star: boolean = false;

  constructor(public navCtrl: NavController, public rest: RestApiProvider, public loading: LoadingController, public menu : MenuController) {
    this.terms = 'apple';
  }

  ionViewDidLoad() {


    this.getFoodReport('water');
  }
  onInput(event) {

    if (event.data) {
      if (this.terms.length >= 3)
        this.getFoodReport(this.terms);
    }
  }
  onCancel(event) {
    console.log(event);
  }
  onClear() {
    console.log('clear');
  }
  onItemClick(item) {
    console.log(item.name);
    this.navCtrl.push(DetailsViewPage, { item: item });
  }

  getFoodReport(term) {
    let loader = this.loading.create({
      content: 'Getting latest report...',
    });
    loader.present().then(() => {
      this.rest.getFoodReport(term)
        .subscribe(
        foods => this.foods = foods,
        error => this.errorMessage = <any>error);
    });
    loader.dismiss();
  }

  addFav(item) : void {
    console.log(item)
    let bookmark = {
      'group': item.group,
      'name': item.name,
      'star': item.star,
      'ndbno' : item.ndbno
    }
    let myfav = JSON.parse(localStorage.getItem("favFoods"));
    if (myfav == null) {
      this.favFoods.push(bookmark);
      window.localStorage.setItem("favFoods", JSON.stringify(this.favFoods));
    } else {
      this.favFoods = myfav;
      this.favFoods.push(bookmark);
      window.localStorage.setItem("favFoods", JSON.stringify(this.favFoods));
    }
    this.star = !this.star;  //show and hide icon on each toggle
  }

  openFav(){
    this.navCtrl.push(FavPage);
  }

  closeMenu(){
    this.menu.close();
  }


}
