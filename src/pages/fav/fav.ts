import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DetailsViewPage } from '../details-view/details-view';


/**
 * Generated class for the FavPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-fav',
  templateUrl: 'fav.html',
})
export class FavPage {

  favFoods : any[];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FavPage');
    this.loadList();
  }

  loadList() {
    this.favFoods = JSON.parse(window.localStorage.getItem("favFoods")) || [];
    console.log(this.favFoods);
  }

  goToDetail(item){
    console.log(item.name);
    this.navCtrl.push(DetailsViewPage, { item: item });
  }

 
    
 

}
