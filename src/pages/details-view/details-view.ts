import { Component } from '@angular/core';

import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { RestApiProvider } from '../../providers/rest-api/rest-api';


/**
 * Generated class for the DetailsViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-details-view',
  templateUrl: 'details-view.html',
})
export class DetailsViewPage{
  public foodItem: any;
  public itemReport : any
  
  myBookmark: any[];
  constructor(public navCtrl: NavController, public navParams: NavParams, private rest : RestApiProvider, public loading: LoadingController) {
    this.foodItem = navParams.get("item");
  }
 

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsViewPage');
    console.log(this.foodItem);
  
    let loader = this.loading.create({
      content: 'Getting Nutrients...',
    });
      loader.present().then(() => {
      this.rest.getViewReport(this.foodItem.ndbno).subscribe(res => this.itemReport = res.report.food.nutrients );
    })
    loader.dismiss();
  }

  
}
