import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class RestApiProvider {

  private apiUrl = 'https://api.nal.usda.gov/ndb/search/?format=json&sort=n&max=50&offset=0&api_key=kMsdLS3WOow3M54fhiPkvFYEYKTUCHfqMzyARnRe';

  constructor(public http: HttpClient) {}

  getFoodReport(term): Observable<string[]> {
    
    return this.http.get(this.apiUrl+'&q='+term).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  getViewReport(ndbno): Observable<any> {
    let url = "https://api.nal.usda.gov/ndb/reports/?ndbno="+ndbno+"&type=b&format=json&api_key=kMsdLS3WOow3M54fhiPkvFYEYKTUCHfqMzyARnRe"
    return this.http.get(url).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
